import imgLogo from '../assets/images/movie.png';
import imgPlay from '../assets/images/googleplay.png';
import imgApp from '../assets/images/appstore.png';
import imgIg from '../assets/images/instagram.png';
import imgPin from '../assets/images/pinterest.png';
import imgFb from '../assets/images/facebook.png';

export const color = {
    white: '#ffffff',
    black: '#000000',
    red: '#FE024E',
    pink: '#EB507F'
}

export const Picture = {
    Logo: imgLogo,
    Ig: imgIg,
    Play: imgPlay,
    App: imgApp,
    Pin: imgPin,
    Fb: imgFb
}