import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getMovies,
  getGenres,
  getMoviesByGenre,
} from "../../redux/Action/movie";
import { Container, Button } from "reactstrap";
import Carousel from "../../components/Carousel/index";
import { Link, useHistory } from "react-router-dom";
import _ from "lodash";
import jwt_decode from "jwt-decode";

const HomePage = (props) => {
  const imgUrl = "https://image.tmdb.org/t/p/w500";

  const [page, setPage] = useState("");

  const handleChangePage = (pg) => {
    // setPage(pg);
    // props.getMovies(pg);
  };
  useEffect(() => {
    props.getMovies();
  }, []);

  useEffect(() => {
    props.getGenres();
  }, []);

  let history = useHistory();
  const handleMovie = () => {
    history.push("/add-movies");
  };

  const token = localStorage.getItem("token");
  let decoded;
  if (token && !_.isEmpty(token)) decoded = jwt_decode(token);

  return (
    <>
      <Carousel />
      <Container className="mt-4">
        {props.genres.length !== 0 ? (
          props.genres.map((genre) => (
            <Button
              key={genre.id}
              onClick={() => props.getMoviesByGenre(genre.id)}
              className="mr-2 mb-2 rounded-pill"
              outline
              color="danger"
            >
              {genre.name}
            </Button>
          ))
        ) : (
          <div>Loading...</div>
        )}

        {token && decoded.role === 1 ? (
          <div>
            <Button onClick={handleMovie} color="success" className="mt-3 mb-3">
              Add Movie
            </Button>{" "}
          </div>
        ) : (
          ""
        )}

        <div className="movies-list">
          {props.movies !== 0
            ? props.movies.map((movie, index) => (
                <Link
                  to={`/detail-movie/${movie.id}`}
                  style={{ textDecoration: "none", color: "black" }}
                >
                  <div className="movie-item" key={index}>
                    <img src={movie.poster} alt={movie.name} />
                    <h5>{movie.title}</h5>
                    <p>
                      {/* {props.genres.map((genre) =>
                        genre.id === movie.genre_ids[1] ? genre.name : ""
                      )} */}
                    </p>
                  </div>
                </Link>
              ))
            : ""}
        </div>
        <div className="home-pagination my-4 d-flex justify-content-center">
          {page !== 1 && (
            <Button
              onClick={() => handleChangePage(page - 1)}
              color="danger"
              className=" mr-2"
            >
              Previous
            </Button>
          )}
          <Button
            onClick={() => handleChangePage(1)}
            color={page === 1 ? "danger" : "light"}
            className="rounded-circle mr-2"
          >
            1
          </Button>
          <Button
            onClick={() => handleChangePage(2)}
            color={page === 2 ? "danger" : "light"}
            className="rounded-circle mr-2"
          >
            2
          </Button>
          <Button
            onClick={() => handleChangePage(3)}
            color={page === 3 ? "danger" : "light"}
            className="rounded-circle mr-2"
          >
            3
          </Button>
          <Button
            onClick={() => handleChangePage(4)}
            color={page === 4 ? "danger" : "light"}
            className="rounded-circle mr-2"
          >
            4
          </Button>
          <Button
            onClick={() => handleChangePage(5)}
            color={page === 5 ? "danger" : "light"}
            className="rounded-circle mr-2"
          >
            5
          </Button>
          <Button
            onClick={() => handleChangePage(page + 1)}
            color="danger"
            className=" mr-2"
          >
            Next
          </Button>
        </div>
      </Container>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    movies: state.movie.movies,
    genres: state.movie.genres,
    id: state.movie.id,
    // input: state.input,
    // movieSearch: state.movieSearch,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMovies: () => dispatch(getMovies()),
    getGenres: () => dispatch(getGenres()),
    getMoviesByGenre: (id) => dispatch(getMoviesByGenre(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
