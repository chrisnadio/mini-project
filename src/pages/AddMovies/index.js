import React, { useState } from "react";
import { connect } from "react-redux";
import { addMovieAction } from "../../redux/Action/movie";
import {
  Container,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

function AddMovie(props) {
  const [addMovie, setAddMovie] = useState({
    title: "",
    release_date: "",
    director: "",
    budget: "",
    featured_song: "",
    synopsis: "",
  });

  const poster =
    "https://m.media-amazon.com/images/M/MV5BMGIzZmQ4YmUtZGQ4NC00OTkyLWE1MGUtMTQ3N2Y3N2E2NWEyXkEyXkFqcGdeQXVyODAzNzAwOTU@._V1_UY1200_CR85,0,630,1200_AL_.jpg";
  const backdrop =
    "https://i1.wp.com/wallur.com/wp-content/uploads/2016/12/doraemon-background-1.jpg?fit=1920%2C1080";
  const trailer = "https://www.youtube.com/watch?v=NDNUR_czo9I";
  const genre_id = 1;

  const token = localStorage.getItem("token");

  const handleChange = (event) => {
    setAddMovie({
      ...addMovie,
      [event.target.name]: event.target.value,
    });
    // console.log(event);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const body = {
      title: addMovie.title,
      release_date: addMovie.release_date,
      director: addMovie.director,
      budget: addMovie.budget,
      featured_song: addMovie.featured_song,
      synopsis: addMovie.synopsis,
      poster: poster,
      backdrop: backdrop,
      trailer: trailer,
      genre_id: genre_id,
    };
    props.addMovieAction(token, body);
  };
  console.log("Ini", addMovie);
  return (
    <>
      <Container>
        <Form className="mt-3 mb-3">
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Title</Label>
              <Input
                onChange={handleChange}
                type="text"
                name="title"
                id="title"
                placeholder="Input Title"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Release Date</Label>
              <Input
                onChange={handleChange}
                type="text"
                name="release_date"
                id="release_date"
                placeholder="Input Realese Date"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Director</Label>
              <Input
                onChange={handleChange}
                type="text"
                name="director"
                id="director"
                placeholder="Input Director"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Budget</Label>
              <Input
                onChange={handleChange}
                type="text"
                name="budget"
                id="budget"
                placeholder="Insert Budget"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Featured Song</Label>
              <Input
                onChange={handleChange}
                type="text"
                name="featured_song"
                id="feautured_song"
                placeholder="Input Song"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleEmail">Synopsis</Label>
              <Input
                onChange={handleChange}
                type="textarea"
                name="synopsis"
                id="synopsis"
                placeholder="Insert Synopsis"
              />
            </FormGroup>
          </Col>
          <Button onClick={handleSubmit}>Add Movie</Button>
        </Form>
      </Container>
    </>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    addMovieAction: (token, body) => dispatch(addMovieAction(token, body)),
  };
};

export default connect(null, mapDispatchToProps)(AddMovie);
