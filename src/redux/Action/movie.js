import axios from "axios";
import _ from "lodash";

export const getMovies = () => {
  return (dispatch) => {
    return axios
      .get(`https://team-c.herokuapp.com/movies`)
      .then((res) => {
        // if (res.data.results && !_.isEmpty(res.data.results)) {
        //   dispatch({
        //     type: "GET_MOVIES",
        //     payload: res.data.results,
        //   });
        // }
        dispatch({
          type: "GET_MOVIES",
          payload: res.data.data.findMovies,
        });
      })
      .catch((err) => console.log(err));
  };
};

export const getGenres = () => {
  return (dispatch) => {
    return axios
      .get(
        "https://api.themoviedb.org/3/genre/movie/list?api_key=b8ef0f01e1576b7cc5057b3500920495&language=en-US&page=1"
      )
      .then((res) => {
        dispatch({
          type: "GET_GENRES",
          payload: res.data.genres,
        });
      })
      .catch((err) => console.log(err));
  };
};

export const getMoviesByGenre = (id) => {
  return (dispatch) => {
    axios
      .get(
        `https://api.themoviedb.org/3/movie/popular?api_key=b8ef0f01e1576b7cc5057b3500920495&language=en-US&page=1discover/movie?api_key=b8ef0f01e1576b7cc5057b3500920495&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${id}`
      )
      .then((res) => {
        dispatch({
          type: "GET_MOVIES_BY_GENRES",
          payload: res.data.results,
        });
      });
  };
};
export const getMovieById = (id) => {
  return (dispatch) => {
    axios.get(`https://team-c.herokuapp.com/movies/${id}`).then((res) => {
      dispatch({
        type: "GET_MOVIE_BY_ID",
        payload: res.data.data.getDetail,
      });
    });
  };
};

export const getInput = (input) => (dispatch) => {
  dispatch({
    type: "GET_INPUT",
    payload: input,
  });
};

export const searchMovie = (input) => {
  return (dispatch) => {
    axios.get(`https://team-c.herokuapp.com/search/${input}`).then((res) => {
      dispatch({
        type: "SEARCH_MOVIE",
        payload: res.data.data.findMovies,
      });
    });
  };
};

export const getCharacter = () => {
  return (dispatch) => {
    return axios
      .get(
        "https://api.themoviedb.org/3/person/popular?api_key=b8ef0f01e1576b7cc5057b3500920495&language=en-US&page=1"
      )
      .then((res) => {
        dispatch({
          type: "GET_CHARACTER",
          payload: res.data.results,
        });
      });
  };
};

export const getReview = () => (dispatch) => {
  axios.get(`https://team-c.herokuapp.com/movies/1`).then((res) => {
    dispatch({
      type: "GET_REVIEW",
      payload: res.data.data,
    });
  });
};

export const postReview = (token, body) => (dispatch) => {
  const config = {
    headers: { authentication: token },
  };
  axios
    .post(`https://team-c.herokuapp.com/review`, body, config)
    .then((res) => {
      console.log("response", res.data.data.addReview);
      dispatch({
        type: "POST_REVIEWS_BY_MOVIE_ID",
        payload: res.data.data.addReview,
      });
    });
};

export const addMovieAction = (token, body) => (dispatch) => {
  const config = {
    headers: { authentication: token },
  };
  axios
    .post(`https://team-c.herokuapp.com/movies`, body, config)
    .then((res) => {
      console.log("Ini ", res);
      dispatch({
        type: "ADD_MOVIE",
        payload: res.data.data.addMovies,
      });
    });
};
