const initialState = {
  movies: [],
  genres: [],
  movie: [],
  input: "",
  id: 0,
  reviews: [],
  character: [],
  addMovieState: [],
};

const movieReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "GET_MOVIES":
      return {
        ...state,
        movies: payload,
      };
    case "GET_GENRES":
      return {
        ...state,
        genres: payload,
      };
    case "GET_MOVIES_BY_GENRES":
      return {
        ...state,
        movies: payload,
        id: payload,
      };
    case "GET_MOVIE_BY_ID":
      return {
        ...state,
        movie: payload,
      };
    case "SEARCH_MOVIE":
      return {
        ...state,
        movies: payload,
      };
    case "GET_INPUT":
      return {
        ...state,
        input: payload,
      };
    case "GET_CHARACTER":
      return {
        ...state,
        character: payload,
      };
    case "POST_REVIEWS_BY_MOVIE_ID":
      return {
        ...state,
        reviews: [payload, ...state.reviews],
      };
    case "ADD_MOVIE":
      return {
        ...state,
        addMovieState: [payload, ...state.addMovieState],
      };
    default:
      return { ...state };
  }
};

export default movieReducer;
