import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { getMovieById } from "../../redux/Action/movie";
import { Container } from "reactstrap";

function Overview(props) {
  const imgUrl = "https://image.tmdb.org/t/p/w500";
  const { id } = useParams();

  useEffect(() => {
    props.getMovieById(id);
  }, []);

  return (
    <div>
      <Container className="mb-5">
        <div>
          <h3>Synopsys</h3>
          <p>{props.movie.synopsis}</p>
        </div>
        <div className="movie-info">
          <h3>Movie Info</h3>
          <li>
            <span className="bold">Release Date</span> :{" "}
            {props.movie.release_date}
          </li>
          <li>
            <span className="bold">Director</span> : {props.movie.director}
          </li>
          <li>
            <span className="bold">Budget</span> : {props.movie.budget}
          </li>
          <li>
            <span className="bold">Featured Song</span> :{" "}
            {props.movie.featured_song}
          </li>
        </div>
      </Container>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    movie: state.movie.movie,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMovieById: (id) => dispatch(getMovieById(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Overview);
