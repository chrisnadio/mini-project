import React, { useState } from "react";
import ListBtn from "../ListMenu/index";
import { Container, Button } from "reactstrap";

const ListMenu = () => {
  const [btnActive, setBtnActive] = useState({
    overview: true,
    review: false,
    casts: false,
  });

  const [num, setNum] = useState(1);

  const reviewbtn = (e) => {
    setBtnActive({
      overview: false,
      review: true,
      casts: false,
    });
    setNum(3);
  };

  const charsbtn = () => {
    setBtnActive({
      overview: false,
      review: false,
      casts: true,
    });
    setNum(2);
  };

  const detailbtn = () => {
    setBtnActive({
      overview: true,
      review: false,
      casts: false,
    });
    setNum(1);
  };

  return (
    <>
      <div>
        <Container>
          <Button
            color="danger"
            onClick={detailbtn}
            className={
              btnActive.overview
                ? "mr-2 mb-2 rounded-pill button--inactive"
                : "mr-2 mb-2 rounded-pill button--active"
            }
          >
            Overview
          </Button>

          <Button
            color="danger"
            onClick={charsbtn}
            className={
              btnActive.casts
                ? "mr-2 mb-2 rounded-pill button--inactive"
                : "mr-2 mb-2 rounded-pill button--active"
            }
          >
            Character
          </Button>

          <Button
            color="danger"
            onClick={reviewbtn}
            className={
              btnActive.review
                ? "mr-2 mb-2 rounded-pill button--inactive"
                : "mr-2 mb-2 rounded-pill button--active"
            }
          >
            Review
          </Button>
        </Container>
      </div>
      <ListBtn change={num} />
    </>
  );
};

export default ListMenu;
