import React from "react";
import Review from "../Review/index";
import Character from "../Character/index";
import Overview from "../Overview/index";

export default function ListBtn(props) {
  switch (props.change) {
    case 2:
      return <Character />;
    case 3:
      return <Review />;
    default:
      return <Overview />;
  }
}