import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Col, Container, Row } from "reactstrap";
import { getCharacter } from "../../redux/Action/movie";

function Character(props) {
  const imgUrl = "https://image.tmdb.org/t/p/w500";

  useEffect(() => {
    props.getCharacter();
  }, []);

  return (
    <>
      <Container className="mb-4">
        <Row>
          {props.character.map((char) => (
            <Col md="3" className="mb-4">
              <div>
                <p>{char.name}</p>
                <img
                  src={`${imgUrl}${char.profile_path}`}
                  className="char"
                  alt="cast"
                />
              </div>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    character: state.movie.character,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCharacter: () => dispatch(getCharacter()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Character);
