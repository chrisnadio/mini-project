import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieById } from "../../redux/Action/movie";
import { Jumbotron } from "reactstrap";
import { connect } from "react-redux";
import YouTube from "react-youtube";

const Banner = (props) => {
  // const imgUrl = "https://image.tmdb.org/t/p/w500";
  const { id } = useParams();

  const [video, setVideo] = useState(false);

  useEffect(() => {
    props.getMovieById(id);
  }, []);

  const opts = {
    height: "390",
    width: "640",
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
    },
  };

  console.log(props.movie);

  const handleVideo = () => {
    video === false ? setVideo(true) : setVideo(false);
  };

  const _onReady = (e) => {
    e.target.pauseVideo();
  };

  return (
    <Jumbotron
      style={{
        backgroundImage: `url(${props.movie.backdrop})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        opacity: "90%",
        color: "white",
      }}
    >
      <h1 className="display-3">{props.movie.title}</h1>
      {/* <div className="divider"></div> */}
      <hr className="my-2" />
      <p>{props.movie.synopsis}</p>
      <button className="btn btn-danger mb-3" onClick={handleVideo}>
        Watch Trailer
      </button>

      {video ? (
        <YouTube
          videoId={props.movie.trailer.substr(32, 11)}
          opts={opts}
          onReady={_onReady}
        />
      ) : (
        ""
      )}
    </Jumbotron>
  );
};

const mapStateToProps = (state) => {
  return {
    movie: state.movie.movie,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMovieById: (id) => dispatch(getMovieById(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Banner);
