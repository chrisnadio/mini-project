import React from "react";
import { Row, Col } from "reactstrap";
import {
  StyledHeader,
  StyledFooter,
  Divider,
  ImgSocial,
  Container1,
  Img,
  P,
} from "./styled";
import { Picture } from "../../utility/constant";
import "../../assets/css/footer.css";

const Layout = ({ withHeader, withContent, withFooter }) => {
  // const {Content} = Container;
  const year = new Date().getFullYear();

  return (
    <>
      <StyledHeader>
        {withHeader && (
          <>
            <Row justify="space-between">
              <Col span="5">
                <Img src={Picture.Logo} alt="Logo" />
              </Col>
              <Col span="8">
                <p>Search</p>
              </Col>
              <Col span="5">
                <P>Sign In</P>
              </Col>
            </Row>
          </>
        )}
      </StyledHeader>

      <Col>{withContent}</Col>

      <StyledFooter>
        {withFooter && (
          <Container1>
            <Row>
              <Col xs="6">
                <Row>
                  <Img src={Picture.Logo} alt="Logo" />
                  <h1>MilanTV</h1>
                </Row>
                <p>
                  Loren Ipsum is simply dummy text of the printing and type
                  setting industry. Lorem Ipsum has been the industry's
                  standard. printing and typesetting industry. Lorem Ipsum has
                  been the industry's standard.
                </p>
              </Col>
              <Col xs="3">
                <li>Tentang Kami</li>
                <li>Blog</li>
                <li>Layanan</li>
                <li>Karir</li>
                <li>Pusat Media</li>
              </Col>
              <Col>
                Download
                <Row xs="2">
                  <Col>
                    <ImgSocial src={Picture.Play} alt="Google Play" />
                  </Col>
                  <Col>
                    <ImgSocial src={Picture.App} alt="App Store" />
                  </Col>
                </Row>
                Social Media
                <Row xs="4">
                  <Col>
                    <ImgSocial src={Picture.Fb} alt="Facebook" />
                  </Col>
                  <Col>
                    <ImgSocial src={Picture.Pin} alt="Pinterest" />
                  </Col>
                  <Col>
                    <ImgSocial src={Picture.Ig} alt="Instagram" />
                  </Col>
                </Row>
              </Col>
            </Row>
            <Divider />
            <P>Copyright &copy; {year} MilanTV. All Right Reserved</P>
          </Container1>
        )}
      </StyledFooter>
    </>
  );
};

export default Layout;
