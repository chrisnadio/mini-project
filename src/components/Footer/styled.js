import styled from 'styled-components';
import {color} from '../../utility/constant';

export const StyledHeader = styled.div`
    background-color: ${color.white};
`
export const StyledFooter = styled.div`
    background-color: ${color.black};
    color: ${color.white};
    list-style: none;
`
export const Img = styled.img`
    height: 50px;
`
export const ImgSocial = styled.img`
    height: 40px;
`
export const Container1 = styled.div`
    padding: 20px 50px 5px 50px;
`
export const Divider = styled.div`
    border-top: 0.5px solid ${color.white};
    margin: 10px 0px;
`
export const P = styled.p`
    font-size: 15px;
    font-family: Roboto;
    text-align: center;
`
