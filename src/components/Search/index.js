// import React, { useState } from "react";
import React, { useState } from "react";
import search from "../../assets/images/search.png";
import "../../assets/css/search.css";
import { connect } from "react-redux";
import {
  // getMovies,
  // getGenres,
  // getMoviesByGenre,
  searchMovie,
  getInput,
} from "../../redux/Action/movie";

function SearchMovie(props) {
  const [input, setInput] = useState("");
  // const SearchMovie = (props) => {
  //     const [search, setSearch] = useState("");
  //     const [searchResult, setSearchResult] = useState('')

  //     const handleSearch = () => {
  //         let searchResult = props.data.find((item) => item.name === search)

  //         if (searchResult) {
  //             setSearchResult(searchResult)
  //         } else {
  //             setSearchResult('')
  //         }
  //         setSearch('')
  //     }
  //   };

  const handleChange = (e) => {
    setInput(e.target.value);
  };
  // console.log(title);
  const onSubmit = (e) => {
    e.preventDefault();
    // const header = { title: input.name };
    props.searchMovie(input);
    console.log("input ==>", input);
  };

  return (
    <div>
      <form action="" onSubmit={onSubmit}>
        <div className="search-form">
          <table className="element-container">
            <tr>
              <td>
                <input
                  type="text"
                  className="search-input"
                  onChange={(e) => handleChange(e)}
                  name="title"
                  // value={input}
                />
              </td>
              <td>
                <button className="search-button">
                  {" "}
                  <img className="search-logo" src={search} alt="search" />
                </button>
              </td>
            </tr>
          </table>
          {/* <button>Search Movie</button> */}
        </div>
      </form>

      {/* <div className="search-result">
          {searchResult !== "" ? (
              <div>Ada Search</div>
          ) : (
              <div>Tidak Ada Search</div>
          )}
      </div> */}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    // movies: state.movies,
    // genres: state.genres,
    id: state.movie.id,
    input: state.movie.input,
    // movieSearch: state.movieSearch,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // getMovies: () => dispatch(getMovies()),
    // getGenres: () => dispatch(getGenres()),
    // getMoviesByGenre: (id) => dispatch(getMoviesByGenre(id)),
    searchMovie: (body) => dispatch(searchMovie(body)),
    getInput: (input) => dispatch(getInput(input)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchMovie);
