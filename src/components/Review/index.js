import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getMovieById, postReview } from "../../redux/Action/movie";
import Profile from "../../assets/images/movie.png";
import {
  Container,
  Jumbotron,
  Button,
  Spinner,
  Row,
  Col,
  FormGroup,
  Input,
} from "reactstrap";

import ReactStars from "react-stars";

import "../../assets/css/review.css";
import { useParams } from "react-router-dom";

function MovieReview(props) {
  const { id } = useParams();

  const [review, setReview] = useState({
    authentication: "",
    movie_id: "",
    comment: "",
    rating: "",
  });

  const token = localStorage.getItem("token");
  console.log("token =>", token);

  useEffect(() => {
    props.getMovieById(id);
  }, []);

  useEffect(() => {
    console.log(props.movie.reviews);
  }, [props.movie.reviews]);

  const handleChange = (e) => {
    setReview({
      ...review,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (token === null) {
      alert("You must login to add review");
    }

    const body = {
      movie_id: id,
      comment: review.comment,
      rating: "4",
    };
    props.postReview(token, body);
    setReview({
      comment: "",
    });
    props.getMovieById(id);
  };

  console.log(review);

  return (
    <div>
      <Container>
        <Row>
          <Col md={2}>
            <img
              style={{
                width: "100px",
                height: "100px",
                objectFit: "cover",
                borderRadius: "50%",
              }}
              src={Profile}
              alt="cat"
            />
          </Col>
          <Col>
            <div class="author">
              <h4 className="mb-0">{}</h4>
            </div>
            <div class="author-rating mb-1">
              <ReactStars
                size={20}
                edit={true}
                isHalf={false}
                count={5}
                // value={}
              />
            </div>
            <div class="author-review">
              <FormGroup>
                <Input
                  type="textarea"
                  onChange={handleChange}
                  value={review.comment}
                  name="comment"
                />
              </FormGroup>
              <Button onClick={handleSubmit} color="danger">
                Submit Review
              </Button>
            </div>
          </Col>
        </Row>

        {props.movie.reviews
          .map((review) => (
            <Row className="mb-3 mt-3">
              <Col md={2}>
                <img
                  style={{
                    width: "100px",
                    height: "100px",
                    objectFit: "cover",
                    borderRadius: "50%",
                  }}
                  src="https://static.boredpanda.com/blog/wp-content/uploads/2019/10/cinderblock-fat-cat-workout-fb.png"
                  alt="cat"
                />
              </Col>
              <div className="authoe-review">
                <p>{review.comment}</p>
              </div>
            </Row>
          ))
          .reverse()}
      </Container>
    </div>
    // <div>
    //   <div>
    //     <div className="post-comment">
    //       <div className="profile">
    //         <img src={Profile} alt="" />
    //       </div>
    //       <div>
    //         <h5>Yudi Kaka</h5>
    //         <input type="text" className="input-bar" />
    //         <button type="submit" className="submit-btn">
    //           Submit
    //         </button>
    //       </div>
    //     </div>

    //     <div>
    //       {/* <div className="comment">
    //         {props.reviews.length !== 0
    //           ? props.reviews.map((review) => (
    //               <div className="comment-wrapper">
    //                 <div>
    //                   <img src={review.comment} alt="" />
    //                 </div>
    //                 <div>{review.rating}</div>
    //               </div>
    //             ))
    //           : ""} */}
    //       <Container>
    //         {props.movie.reviews.map((review) => review.comment)}
    //       </Container>
    //       <button className="load-more">load more</button>
    //       {/* </div> */}
    //     </div>
    //   </div>
    // </div>
  );
}

const mapStateToProps = (state) => {
  return {
    movie: state.movie.movie,
    // reviews: state.movie.reviews,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMovieById: (id) => dispatch(getMovieById(id)),
    postReview: (token, body) => dispatch(postReview(token, body)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MovieReview);
