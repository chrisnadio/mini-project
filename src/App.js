import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/style.css";
import HomePage from "./pages/HomePages/index";
import MovieDetail from "./pages/MovieDetail";
import Navbar from "./components/Layout/Layout";
import Layout from "./components/Footer";
import AddMovie from "./pages/AddMovies";

function App() {
  return (
    <div className="App">
      <Navbar />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/detail-movie/:id" component={MovieDetail} />
          <Route path="/add-movies/" component={AddMovie} />
        </Switch>
      </BrowserRouter>
      <Layout withFooter />
    </div>
  );
}

export default App;
