# Welcome to mini project-movie 👋
[![Version](https://img.shields.io/npm/v/mini project-movie.svg)](https://www.npmjs.com/package/mini project-movie)
![Prerequisite](https://img.shields.io/badge/npm-%3E%3D5.5.0-blue.svg)
![Prerequisite](https://img.shields.io/badge/node-%3E%3D9.3.0-blue.svg)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)]( https://gitlab.com/binar-10-mini-project/team-c)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://github.com/kefranabg/readme-md-generator/graphs/commit-activity)
[![License: MIT](https://img.shields.io/github/license/timC/mini project-movie)](https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE)

> Mini Project Tim C GA Batch 10

### 🏠 [Homepage]( https://gitlab.com/binar-10-mini-project/team-c)

## Prerequisites

- npm >=5.5.0
- node >=9.3.0

## Install

```sh
(npm install)
```

## Usage

```sh
enjoy the product
```

## Run tests

```sh
npm run test
```

## Author

👤 **(Amrina Aulia Siregar) (Fahmi Aga Aditya) (Fabianus Chrisna Dio)**

* GitHub: [@timC](https://github.com/timC)

## Show your support

Give a ⭐️ if this project helped you!


## 📝 License

Copyright © 2021 [(Amrina Aulia Siregar) (Fahmi Aga Aditya) (Fabianus Chrisna Dio)](https://github.com/timC).

This project is [MIT](https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_